﻿class PhysicsMath
{
    public static double G = 6.67408e-11;
    public static double k = 8.99e9;

    public static decimal GravitationalForce(decimal m1, decimal m2, decimal r)
    {
        return ((decimal)G * m1 * m2) / (r * r);
    }

    public static decimal CoulombForce(decimal q1, decimal q2, decimal r)
    {
        return ((decimal)k * q1 * q2) / (r * r);
    }
}
