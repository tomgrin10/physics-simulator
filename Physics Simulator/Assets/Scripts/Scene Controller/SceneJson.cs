﻿using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using Vexe.Runtime.Extensions;

public class SceneJson : MonoBehaviour
{
    public GameObject StaticObject;
    public GameObject PhysicsObject;

    private JsonSerializerSettings JsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };

    public class ObjectJsonData
    {
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public ObjectSettings Settings { get; set; }
    }

    public class SceneJsonData
    {
        public SceneSettings Settings { get; set; }
        public List<ObjectJsonData> Objects { get; set; }
    }

    [TextArea(5, 15)]
    public string json;

    public string ToJson()
        => JsonConvert.SerializeObject(
            new SceneJsonData
            {
                Settings = GameObject.FindGameObjectWithTag("SceneController").GetComponent<SceneSettings>(),
                Objects = GameObject.FindGameObjectWithTag("EditObjectsContainer").FindAllComponentsDownwards<ObjectSettings>()
                    .Select(o => new ObjectJsonData {
                        Position = o.transform.position,
                        Scale = o.transform.localScale,
                        Settings = o
                    }).ToList()
            },
            Formatting.Indented,
            JsonSettings
        );

    public void SetJson(string json)
    {
        var newScene = JsonConvert.DeserializeObject<SceneJsonData>(json, JsonSettings);
        var sceneSettings = GameObject.FindGameObjectWithTag("SceneController").GetComponent<SceneSettings>();
        
        // Set scene settings
        foreach (FieldInfo field in typeof(SceneSettings).GetFields())
        {
            if (Attribute.IsDefined(field, typeof(JsonPropertyAttribute)))
                field.SetValue(sceneSettings, field.GetValue(newScene.Settings));
        }

        var editObjectsContainer = GameObject.FindGameObjectWithTag("EditObjectsContainer");
        // Delete existing objects
        editObjectsContainer.DestroyChildren();

        // Create new objects
        foreach (ObjectJsonData data in newScene.Objects)
        {
            if (data.Settings is PhysicsObjectSettings)
            {
                var newSettings = Instantiate(PhysicsObject, editObjectsContainer.transform).GetComponent<PhysicsObjectSettings>();
                foreach (FieldInfo field in typeof(PhysicsObjectSettings).GetFields())
                {
                    if (Attribute.IsDefined(field, typeof(JsonPropertyAttribute)))
                        field.SetValue(newSettings, field.GetValue(data.Settings as PhysicsObjectSettings));
                }
                newSettings.gameObject.transform.position = data.Position;
                newSettings.gameObject.transform.localScale = data.Scale;
            }
            else
            {
                var newSettings = Instantiate(StaticObject, editObjectsContainer.transform).GetComponent<ObjectSettings>();
                foreach (FieldInfo field in typeof(ObjectSettings).GetFields())
                {
                    if (Attribute.IsDefined(field, typeof(JsonPropertyAttribute)))
                        field.SetValue(newSettings, field.GetValue(data.Settings));
                }
                newSettings.gameObject.transform.position = data.Position;
                newSettings.gameObject.transform.localScale = data.Scale;
            }

            
        }
    }
}

[CustomEditor(typeof(SceneJson))]
public class SaveSceneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SceneJson myScript = target as SceneJson;

        GUILayout.BeginHorizontal("box");
        if (GUILayout.Button("Get"))
        {
            var json = myScript.ToJson();
            myScript.json = json;
        }
        if (GUILayout.Button("Save"))
        {
            myScript.SetJson(myScript.json);
        }
        GUILayout.EndHorizontal();
    }
}
