﻿using System;
using UnityEditor;
using UnityEngine;
using Vexe.Runtime.Types;

public class ModeManager : MonoBehaviour, IRunModeToggleHandler, IEditModeToggleHandler
{
    public enum Mode { Edit, Run }
    
    [ReadOnly]
    public Mode mode = Mode.Edit;

    private GameObject editContainer, runContainer;

    private void Awake()
    {
        editContainer = GameObject.FindGameObjectWithTag("EditObjectsContainer");
        runContainer = GameObject.FindGameObjectWithTag("RunObjectsContainer");
        ToggleModeGlobally(Mode.Edit);
    }

    public void ToggleModeGlobally()
        => ToggleModeGlobally(mode == Mode.Edit ? Mode.Run : Mode.Edit);

    public void ToggleModeGlobally(Mode mode)
    {
        this.mode = mode;

        if (mode == Mode.Edit) // Toggle Edit
            Utils.ExecuteEventToAllObjects<IEditModeToggleHandler>(x => x.ToggleEditMode());
        else    // Toggle Run
            Utils.ExecuteEventToAllObjects<IRunModeToggleHandler>(x => x.ToggleRunMode());
    }

    public void ToggleEditMode()
    {
        GameObject runContainer = GameObject.FindGameObjectWithTag("RunObjectsContainer");
        // Delete run objects
        foreach (Transform t in runContainer.transform)
            Destroy(t.gameObject);
        // Enable edit objects
        editContainer.SetActive(true);
    }

    public void ToggleRunMode()
    {
        // Disable edit objects
        editContainer.SetActive(false);

        // Go through all runnable objects in edit container
        foreach (IRunnable iRunnable in editContainer.FindAllComponentsDownwards<IRunnable>())
        {
            // Create run object and put it in run container
            iRunnable.CreateRunObject().transform.parent = runContainer.transform;
        }
    }
}

[CustomEditor(typeof(ModeManager))]
public class ModeManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ModeManager myScript = target as ModeManager;
        
        if (GUILayout.Button(myScript.mode == ModeManager.Mode.Edit ? ModeManager.Mode.Run.ToString() : ModeManager.Mode.Edit.ToString()))
        {
            myScript.ToggleModeGlobally();
        }
    }
}