﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vexe.Runtime.Types;

public class TimeController : BaseBehaviour, IRunModeToggleHandler
{
    [Show, iSlider(10, 400), OnChanged("ScaleTime")]
    private int timeScale = 100;
    [Show, OnChanged("TogglePause")]
    private bool paused;

    [Header("GUI")]
    public Slider timeScaleSlider;
    public InputField timeScaleInputField;
    public TimeControllerButton PlayPauseButton;

	public void ScaleTime(int timeScale)
	{
	    timeScale = Mathf.Clamp(timeScale, 10, 400);
        Time.timeScale = paused ? 0 : timeScale / 100f;
        this.timeScale = timeScale;
        timeScaleSlider.value = timeScale;
        timeScaleInputField.text = timeScale.ToString();
    }

    public void TogglePause()
        => TogglePause(!paused);

    public void TogglePause(bool paused)
    {
        this.paused = paused;
        
        if (paused)
            Time.timeScale = 0;
        else
            ScaleTime(timeScale);

        PlayPauseButton.SetIcon(paused);
    }

    public void ToggleRunMode()
    {
        TogglePause(false);
        ScaleTime(100);
    }
}
