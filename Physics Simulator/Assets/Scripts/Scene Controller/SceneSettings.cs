﻿using UnityEngine;
using Vexe.Runtime.Types;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class SceneSettings : BaseBehaviour, IRunModeToggleHandler
{
    [OnChanged(Set = "_groundSetter"), JsonProperty]
    public bool ground;
    [VisibleWhen("ground"), OnChanged(Set = "_frictionCoefficientSetter"), JsonProperty]
    public float frictionCoefficient;
    [VisibleWhen("ground"), OnChanged(Set = "_bouncinessSetter"), JsonProperty]
    public float bounciness;
    [VisibleWhen("ground"), JsonProperty]
    public float gravity = 9.8f;

    // Setters
    public bool _groundSetter
    {
        get { return ground; }
        set { boundariesController.gameObject.SetActive(value); }
    }
    private float _frictionCoefficientSetter
    {
        get { return frictionCoefficient; }
        set { boundariesController.frictionCoefficient = value; }
    }
    private float _bouncinessSetter
    {
        get { return bounciness; }
        set { boundariesController.bounciness = value; }
    }

    // Private variables and initialization
    private BoundariesController boundariesController;
    private void Awake()
    {
        boundariesController = GameObject.FindGameObjectWithTag("Boundaries").GetComponent<BoundariesController>();
    }

    // Apply all the settings
    public void ToggleRunMode()
    {
        // Set the gravity
        Physics2D.gravity = ground ? new Vector2(0, -gravity) : new Vector2();
    }
}