﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControllerPanel : MonoBehaviour, IRunModeToggleHandler, IEditModeToggleHandler
{
    public void ToggleRunMode()
    {
        gameObject.SetActive(true);
    }

    public void ToggleEditMode()
    {
        gameObject.SetActive(false);
    }
}
