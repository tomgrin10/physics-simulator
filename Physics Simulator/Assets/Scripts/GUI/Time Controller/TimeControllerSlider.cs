﻿using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class TimeControllerSlider : MonoBehaviour
{
    public void ScaleTime()
        => GameObject.FindGameObjectWithTag("SceneController").GetComponent<TimeController>()
            .ScaleTime((int)GetComponent<Slider>().value);
}
