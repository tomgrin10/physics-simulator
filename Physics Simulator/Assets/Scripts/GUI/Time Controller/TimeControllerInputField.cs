﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeControllerInputField : MonoBehaviour
{
    public void ScaleTime()
        => GameObject.FindGameObjectWithTag("SceneController").GetComponent<TimeController>()
            .ScaleTime(int.Parse(GetComponent<InputField>().text));
}
