﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeControllerButton : MonoBehaviour
{
    public Sprite playIcon;
    public Sprite pauseIcon;

    public void SetIcon(bool paused)
        => GetComponent<Image>().sprite = paused ? playIcon : pauseIcon;
}
