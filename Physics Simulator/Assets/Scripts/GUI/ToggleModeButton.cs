﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleModeButton : MonoBehaviour, IEditModeToggleHandler, IRunModeToggleHandler
{
    public Sprite editButtonSprite;
    public Sprite runButtonSprite;

    public void ToggleEditMode()
    {
        GetComponent<Image>().sprite = runButtonSprite;
    }

    public void ToggleRunMode()
    {
        GetComponent<Image>().sprite = editButtonSprite;
    }
}
