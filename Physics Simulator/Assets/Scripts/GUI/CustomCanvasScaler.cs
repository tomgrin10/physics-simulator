﻿using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class CustomCanvasScaler : MonoBehaviour, IScreenResizeHandler
{
    public Vector2Int referenceResoultion = new Vector2Int(800, 600);
    public float scaleFactor;

    private void Awake()
    {
        OnScreenResize();
    }

    public void OnScreenResize()
    {
        float referenceSize = Mathf.Sqrt(referenceResoultion.x * referenceResoultion.x +
                                         referenceResoultion.y * referenceResoultion.y);
        float newSize = Mathf.Sqrt(Screen.width * Screen.width +
                                   Screen.height * Screen.height);

        float ratio = newSize / referenceSize;
        float newScale = (ratio + 1) * scaleFactor;
        GetComponent<CanvasScaler>().scaleFactor = newScale;
    }
}
