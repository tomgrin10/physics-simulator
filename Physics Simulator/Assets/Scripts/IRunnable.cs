﻿using UnityEngine;

interface IRunnable
{
    GameObject CreateRunObject();
}
