﻿public class Defaultable<T>
{
    public T Value;
    public T DefaultValue;
}