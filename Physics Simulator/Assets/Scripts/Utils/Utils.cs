﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Vexe.Runtime.Extensions;

public static class Utils
{
    // Executes the given function with the given data for the object
    public static void ExecuteEvent<T>(this GameObject target, Action<T> functor)
    {
        target.GetComponents<T>().Foreach(functor);
    }

    // Executes the given function with the given data for the object and its children and their children...
    public static void ExecuteEventDownwards<T>(this GameObject target, Action<T> functor)
    {
        foreach (Transform trans in target.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.ExecuteEvent(functor);
        }
    }

    // Executes the given function with the given data for all objects
    public static void ExecuteEventToAllObjects<T>(Action<T> functor)
    {
        GameObject.FindGameObjectWithTag("Root").ExecuteEventDownwards(functor);
    }

    // Returns a list of all components of type T of object and its children and their children...
    public static IEnumerable<T> FindAllComponentsDownwards<T>(this GameObject target)
    {
        foreach (Transform trans in target.GetComponentsInChildren<Transform>(true))
        {
            T t = trans.gameObject.GetComponent<T>();
            if (t != null)
                yield return t;
        }
    }

    // Gives height of camera in orthographic mode
    public static float OrthographicHeight(this Camera camera)
    {
        return camera.orthographicSize * 2;
    }

    // Gives width of camera in orthographic mode
    public static float OrthographicWidth(this Camera camera)
    {
        return camera.OrthographicHeight() * camera.aspect;
    }

    // Returns sum vector of vector list
    public static Vector2 Sum(this IEnumerable<Vector2> arr)
    {
        Vector2 res = new Vector2();
        foreach (var v in arr)
            res += v;
        return res;
    }

    public static void AddForces(this Rigidbody2D rb, IEnumerable<Vector2> forces, ForceMode2D mode = ForceMode2D.Force)
    {
        foreach (var force in forces)
            rb.AddForce(force, mode);
    }
}

