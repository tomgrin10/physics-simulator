﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vexe.Runtime.Types;

public class CameraFollow : BaseBehaviour
{
    [OnChanged(Set = "ToFollow")]
    public GameObject toFollow;
    private GameObject ToFollow
    {
        get { return toFollow; }
        set { GetComponent<CameraZoom>().zoomToMousePosition = (value == null); }
    }

    private void Update()
    {
        if (toFollow != null)
        {
            transform.position = new Vector3(toFollow.transform.position.x, toFollow.transform.position.y, transform.position.z);
        }
    }
}
