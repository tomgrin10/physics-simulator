﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public float zoomChange;
    public float minZoom;
    public float maxZoom;
    public bool zoomToMousePosition = true;

    private Camera cam;

    private void Awake()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        float zoom = Input.GetAxis("Zoom");
        if (zoom != 0)
        {
            Zoom(-zoom);
        }
	}

    public void Zoom(float zoom)
    {
        // Get coords of position change
        Vector2 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        float xRatio = (mousePos.x - (transform.position.x - cam.OrthographicWidth() / 2))
            / (transform.position.x + cam.OrthographicWidth() / 2 - mousePos.x);
        float yRatio = (mousePos.y - (transform.position.y - cam.OrthographicHeight() / 2))
            / (transform.position.y + cam.OrthographicHeight() / 2 - mousePos.y);

        // Set size of camera
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize + zoomChange * zoom, minZoom, maxZoom);

        // Change position
        if (zoomToMousePosition)
        {
            float xLeft = cam.OrthographicWidth() / (1 / xRatio + 1);
            float yBottom = cam.OrthographicHeight() / (1 / yRatio + 1);
            transform.position = new Vector3(
                mousePos.x - xLeft + cam.OrthographicWidth() / 2,
                mousePos.y - yBottom + cam.OrthographicHeight() / 2,
                transform.position.z);
        }
    }
}
