﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraColliderController : MonoBehaviour
{
    public float space = 1;
    private Camera cam;
    private BoxCollider2D camCollider;
    private BoundariesController boundaries;

    private void Awake()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        camCollider = GetComponent<BoxCollider2D>();
        boundaries = GameObject.FindGameObjectWithTag("Boundaries").GetComponent<BoundariesController>();
    }

    private void LateUpdate()
    {
        UpdateColliderSize();
    }

    public void ForceCollison()
    {
        float camWidth = cam.OrthographicWidth();
        float camHeight = cam.OrthographicHeight();

        Vector3 newPosition = transform.position;
        if (boundaries.isLeftWallFilled)
            newPosition.x = Mathf.Max(-boundaries.leftLength - space, newPosition.x - camWidth / 2) + camWidth / 2;
        if (boundaries.isRightWallFilled)
            newPosition.x = Mathf.Min(boundaries.rightLength + space, newPosition.x + camWidth / 2) - camWidth / 2;
        if (boundaries.isGroundFilled)
            newPosition.y = Mathf.Max(-space, newPosition.y - camHeight / 2) + camHeight / 2;
        if (boundaries.isCeilingFilled)
            newPosition.y = Mathf.Min(boundaries.height + space, newPosition.y + camHeight / 2) - camHeight / 2;

        transform.position = newPosition;
    }

    public void UpdateColliderSize()
    {
        camCollider.size = new Vector2(cam.OrthographicWidth(), cam.OrthographicHeight());
        if (boundaries.gameObject.activeInHierarchy)
            ForceCollison();
    }
}
