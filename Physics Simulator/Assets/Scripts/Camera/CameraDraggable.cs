﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDraggable : MonoBehaviour
{
    public float speed = 1;

    private Vector2 prevMousePos;

    private void OnMouseDown()
    {
        if (enabled)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            prevMousePos = mousePos;
        }
    }
    
    private void OnMouseDrag()
    {
        if (enabled)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.Translate((mousePos - prevMousePos) * -speed);
            prevMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}
