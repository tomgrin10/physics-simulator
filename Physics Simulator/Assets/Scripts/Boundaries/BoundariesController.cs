﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vexe.Runtime.Types;

public class BoundariesController : BaseBehaviour, IRunModeToggleHandler
{
    public float growFactor;
    public float thickness;

    [Comment("Assets")]
    public GameObject background;
    public GameObject filledWall;
    public GameObject unfilledWall;

    [Comment("Filling")]
    public bool isGroundFilled;
    public bool isCeilingFilled;
    public bool isLeftWallFilled;
    public bool isRightWallFilled;

    [Comment("Ground Settings")]
    [VisibleWhen("isGroundFilled")]
    public float frictionCoefficient;
    [VisibleWhen("isGroundFilled")]
    public float bounciness;

    [Hide]
    public float height, leftLength, rightLength;
    private GameObject ground, ceiling, leftWall, rightWall;
    
    // Initialize all the game objects
    private void Start()
    {
        // Set up values
        Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        height = cam.OrthographicHeight() * 3;
        leftLength = rightLength = cam.OrthographicWidth() * 2;

        // Background
        background = Instantiate(background, transform);
        background.name = "background";

        // Ground
        ground = Instantiate(isGroundFilled ? filledWall : unfilledWall, transform);
        ground.name = "ground";
        ground.GetComponent<Collider2D>().sharedMaterial = new PhysicsMaterial2D();

        // Ceiling
        ceiling = Instantiate(isCeilingFilled ? filledWall : unfilledWall, transform);
        ceiling.name = "ceiling";

        // Left Wall
        leftWall = Instantiate(isLeftWallFilled ? filledWall : unfilledWall, transform);
        leftWall.name = "left_wall";

        // Right Wall
        rightWall = Instantiate(isRightWallFilled ? filledWall : unfilledWall, transform);
        rightWall.name = "right_wall";

        UpdateBoundaries();
    }

    public void ToggleRunMode()
    {
        // Set the ground stuff
        ground.GetComponent<Collider2D>().sharedMaterial.friction = frictionCoefficient;
        ground.GetComponent<Collider2D>().sharedMaterial.bounciness = bounciness;
        ground.GetComponent<Collider2D>().enabled = false;
        ground.GetComponent<Collider2D>().enabled = true;
    }

    // Update the postions and scales of the game objects
    public void UpdateBoundaries()
    {
        float middle = -leftLength + (leftLength + rightLength) / 2;

        // Background
        background.transform.localScale = new Vector2(rightLength + leftLength, height);
        background.transform.position = new Vector2(middle, height / 2);

        // Ground
        ground.transform.localScale = new Vector2(leftLength + rightLength + thickness * 2, thickness);
        ground.transform.position = new Vector2(middle, -thickness / 2);

        // Ceiling
        ceiling.transform.localScale = new Vector2(leftLength + rightLength + thickness * 2, thickness);
        ceiling.transform.position = new Vector2(middle, height + thickness / 2);

        // Left Wall
        leftWall.transform.localScale = new Vector2(thickness, height);
        leftWall.transform.position = new Vector2(-leftLength - thickness / 2, height / 2);

        // Right Wall
        rightWall.transform.localScale = new Vector2(thickness, height);
        rightWall.transform.position = new Vector2(rightLength + thickness / 2, height / 2);
    }

    public void OnTrigger(GameObject obj)
    {
        if (obj == ground)  // Ground
        {
            // TODO
        }
        else if (obj == ceiling)    // Ceiling
            height *= growFactor;

        else if (obj == leftWall)    // Left Wall
            leftLength *= growFactor;

        else if (obj == rightWall)  // Right Wall
            rightLength *= growFactor;

        UpdateBoundaries();
    }
}
