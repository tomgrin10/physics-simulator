﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundariesTriggerController : MonoBehaviour
{
    private void OnTriggerEnter2D()
    {
        transform.parent.GetComponent<BoundariesController>().OnTrigger(gameObject);
    }
}
