﻿using UnityEngine.EventSystems;

public interface IEditModeToggleHandler : IEventSystemHandler
{
    void ToggleEditMode();
}
