﻿using UnityEngine.EventSystems;

public interface IScreenResizeHandler : IEventSystemHandler
{
    void OnScreenResize();
}