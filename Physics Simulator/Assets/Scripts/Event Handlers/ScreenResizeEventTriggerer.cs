﻿using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class ScreenResizeEventTriggerer : MonoBehaviour
{
    private int prevScreenWidth;
    private int prevScreenHeight;

    private void Awake()
    {
        prevScreenWidth = Screen.width;
        prevScreenHeight = Screen.height;
        Utils.ExecuteEventToAllObjects<IScreenResizeHandler>(x => x.OnScreenResize());
    }

    private void Update()
    {
        if (Screen.width != prevScreenWidth || Screen.height != prevScreenHeight)
        {
            Utils.ExecuteEventToAllObjects<IScreenResizeHandler>(x => x.OnScreenResize());
            prevScreenWidth = Screen.width;
            prevScreenHeight = Screen.height;
        }
    }
}