﻿using UnityEngine.EventSystems;

public interface IRunModeToggleHandler : IEventSystemHandler
{
    void ToggleRunMode();
}
