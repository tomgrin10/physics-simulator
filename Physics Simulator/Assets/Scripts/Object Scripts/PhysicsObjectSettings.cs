﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vexe.Runtime.Types;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class PhysicsObjectSettings : ObjectSettings
{
    [JsonProperty]
    public float mass;
    [JsonProperty]
    public List<Vector2> velocity;
    [JsonProperty]
    public List<Vector2> forces;
    [JsonProperty]
    public List<Vector2> acceleration;

    public override GameObject CreateRunObject()
    {
        GameObject obj = base.CreateRunObject();

        obj.AddComponent<Rigidbody2D>();
        obj.GetComponent<Rigidbody2D>().velocity = velocity.Sum();
        obj.AddComponent<ConstantForce2D>();
        obj.GetComponent<ConstantForce2D>().force = forces.Sum();

        return obj;
    }
}
