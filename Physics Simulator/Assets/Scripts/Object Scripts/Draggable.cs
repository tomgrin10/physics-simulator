﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    private Vector2 positionRelativeToCenter;

    private void OnMouseDown()
    {
        if (enabled)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            positionRelativeToCenter = mousePos - (Vector2)transform.position;
        }
    }

    private void OnMouseDrag()
    {
        if (enabled)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var newPosition = mousePos - positionRelativeToCenter;
            transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);
        }
    }
}
