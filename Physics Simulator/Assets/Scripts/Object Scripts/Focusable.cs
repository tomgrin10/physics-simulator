﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Focusable : MonoBehaviour
{
    private void OnMouseDown()
    {
        GameObject.FindGameObjectWithTag("ObjectInfoPanel").GetComponent<ObjectSettingsPanel>().settings =
            GetComponent<ObjectSettings>();
    }
}
