﻿using UnityEngine;
using Vexe.Runtime.Types;
using Newtonsoft.Json;
using Vexe.Runtime.Extensions;

[JsonObject(MemberSerialization.OptIn)]
public class ObjectSettings : BaseBehaviour, IRunnable
{
    [Header("Constants")]
    public Sprite rectangleSprite;
    public Sprite circleSprite;

    public enum Shape
    {
        Rectangle,
        Circle
    }

    [Header("Values")]
    [JsonProperty("name")]
    public string objectName;

    [JsonProperty, OnChanged(Set ="shape"), Show]
    private Shape _shape;
    public Shape shape
    {
        get { return _shape; }
        set
        {
            ChangeShape(value);
            _shape = value;
        }
    }

    [JsonProperty, OnChanged(Set = "radius"), Show]
    private float _radius = 1;
    public float radius
    {
        get { return _radius; }
        set
        {
            if (shape == Shape.Circle)
                transform.localScale = new Vector2(radius * 2, radius * 2);
        }
    }

    [JsonProperty]
    public float frictionCoefficient;
    [JsonProperty]
    public float bounciness;

    public virtual GameObject CreateRunObject()
    {
        GameObject obj = Instantiate(gameObject);
        // Destroy not needed components
        Destroy(obj.GetComponent<Draggable>());
        Destroy(obj.GetComponent<ObjectSettings>());

        // Set friction and bounciness
        obj.GetComponent<Collider2D>().sharedMaterial = new PhysicsMaterial2D()
        {
            friction = frictionCoefficient,
            bounciness = bounciness
        };

        return obj;
    }

    public void ChangeShape(Shape shape)
    {
        if (shape == Shape.Rectangle)
        {
            GetComponent<CircleCollider2D>().Destroy();
            gameObject.AddComponent<BoxCollider2D>();
            GetComponent<SpriteRenderer>().sprite = rectangleSprite;
        }
        else
        {
            GetComponent<BoxCollider2D>().Destroy();
            gameObject.AddComponent<CircleCollider2D>();
            transform.localScale = new Vector2(radius * 2, radius * 2);
            GetComponent<SpriteRenderer>().sprite = circleSprite;
        }
    }
}
